﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CRUD_AgenciaPublicidad
{
    // CRUD CLIENTES
    public class Clientes
    {
        public string clinombre { get; set; }
        public string cliapellido { get; set; }
        public string cliemail { get; set; }
        public int codigo_cliente { get; set; }
        public string empresa_contrato { get; set; }
        public string tipo_cliente { get; set; }

        // CONSTRUCTOR VACIO
        public Clientes()
        {
        }

        // CONSTRUCTOR CLIENTES //
        public Clientes(string cnombre, string capellido, string cemail, int ccodigo, string cempresa, string ctipo)
        {
            this.clinombre = cnombre;
            this.cliapellido = capellido;
            this.cliemail = cemail;
            this.codigo_cliente = ccodigo;
            this.empresa_contrato = cempresa;
            this.tipo_cliente = ctipo;
        }

        // METODO 1. VERIFICAR NOMBRE VALIDO. 
        public void NombreCli()
        {
            if (clinombre.Contains("1234567890"))
            {
                Console.WriteLine("\n");
                Console.WriteLine("por favor introduzca un Nombre valido!");
            }
            else
            {
                Console.WriteLine("\n");
                Console.WriteLine("Nombre es valido!");
            }
        }
        // FIN METODO 1. VERIFICAR NOMBRE VALIDO. 

        // METODO 2. VERIFICAR APELLIDO VALIDO. 
        public string ApellidoCli()
        {
            if (cliapellido.Contains("1234567890"))
            {
                Console.WriteLine("\n");
                return ("por favor introduzca un Apellido valido!");
            }
            else
            {
                
                return ("Apellido es valido!");
            }
        }
        // FIN METODO 2. VERIFICAR APELLIDO VALIDO. 

        // METODO 3. VERIFICAR EMAIL VALIDO
        public bool EmailCli()
        {
            string expresion;
            expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if (Regex.IsMatch(cliemail, expresion))
            {
                if (Regex.Replace(cliemail, expresion, string.Empty).Length == 0)
                {
                    Console.WriteLine("Email es valido !");
                    Console.WriteLine("\n");
                    return true;
                }
                else
                {
                    Console.WriteLine("Email no es valido !");
                    Console.WriteLine("\n");
                    return false;
                }
            }
            else
            {
                Console.WriteLine("\n");
                Console.WriteLine("Error algo salio mal, intente nuevamente. ");
                return false;
            }
        }
        // FIN METODO 3. VERIFICAR EMAIL VALIDO.


        // METODO VERIFICAR EMPRESA VALIDA //


        // METODO VERIFICAR CODIGO //




    }
}
