﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD_AgenciaPublicidad
{
    public abstract class ProcesosClientes : ProcesosAdministradores
    {
        List<Clientes> datasetcli = new List<Clientes>();
        string clinombre, cliapellido, cliemail, empresa_contrato, tipo_cliente;
        int codigo_cliente;

        // METODO CREACION DE CLIENTES //
        public void Crear_Cliente()
        {
            Console.WriteLine("\n");
            Console.WriteLine("\n");
            Console.WriteLine("--------- MENU CREACION CLIENTES ---------");
            Console.WriteLine("\n");
            Console.WriteLine("INGRESE NOMBRE CLIENTE: ");
            clinombre = Console.ReadLine();
            Console.WriteLine("\n");
            Console.WriteLine("INGRESE APELLIDO CLIENTE: ");
            cliapellido = Console.ReadLine();
            Console.WriteLine("\n");
            Console.WriteLine("INGRESE EMAIL CLIENTE: ");
            cliemail = Console.ReadLine();
            Console.WriteLine("\n");
            Console.WriteLine("INGRESE CODIGO CLIENTE: ");
            codigo_cliente = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("\n");
            Console.WriteLine("INGRESE EMPRESA CON CONTRATO: ");
            empresa_contrato = Console.ReadLine();
            Console.WriteLine("\n");
            Console.WriteLine("DETERMINE TIPO DE CLIENTE: ");
            tipo_cliente = Console.ReadLine();
            datasetcli.Add(new Clientes(clinombre, cliapellido, cliemail, codigo_cliente, empresa_contrato, tipo_cliente));
            Console.WriteLine("\n");
            Console.WriteLine("DATOS DE CLIENTES HAN SIDO GUARDADOS! ");

        }

        // METODOS DE LISTAS CLIENTES //
        private bool Lista_Vacia_cli()
        {
            if (datasetcli.Count == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        ///////////////////////////////////////////////////////////
        public void ListarCli()
        {
            if (Lista_Vacia_cli() == true)
            {
                Console.WriteLine("\n");
                Console.WriteLine("\n");
                Console.WriteLine("No hay datos disponibles en la lista!. ");

            }
            else
            {
                Console.WriteLine("\n");
                Console.WriteLine("TOTAL CLIENTES:  " + datasetcli.Count);
                Console.WriteLine("\n");
                Console.WriteLine(" ------------------------- LISTA ------------------------- ");
                foreach (Clientes item in datasetcli)
                {
                    Mostrar_Datos_cli(item);
                }

            }
            Console.WriteLine("\n");
        }

        //  METODO MOSTRAR DATOS CLIENTES //
        public void Mostrar_Datos_cli(Clientes datocli)
        {
            Console.WriteLine("\n");
            Console.WriteLine("--------------------------------------------------");
            Console.WriteLine("| Nombre: {0} |", datocli.clinombre);
            Console.WriteLine("| Apellido: {0} |", datocli.cliapellido);
            Console.WriteLine("| Email: {0} |", datocli.cliemail);
            Console.WriteLine("| Codigo: {0} |", datocli.codigo_cliente);
            Console.WriteLine("| Empresa Contrato: {0} |", datocli.empresa_contrato);
            Console.WriteLine("| Tipo Cliente: {0} |", datocli.tipo_cliente);
            datocli.NombreCli();
            datocli.ApellidoCli();
            datocli.EmailCli();
        }

        // METODO ELIMINAR DATOS CLIENTE //
        public void Eliminar_Datos_cli()
        {
            try
            {
                string buscarcli;
                if (Lista_Vacia_cli() == true)
                {
                    Console.WriteLine("\n");
                    Console.WriteLine("\n");
                    Console.WriteLine("No hay datos disponibles en la lista!. ");
                }
                else
                {
                    Console.WriteLine("Buscar Nombre Cliente para eliminar: ");
                    buscarcli = Console.ReadLine();
                    foreach (var item in datasetcli)
                    {
                        if (buscarcli == item.clinombre)
                        {
                            Console.WriteLine("--------------------------------------------------");
                            Console.WriteLine("| Nombre: {0} |", item.clinombre);
                            Console.WriteLine("| Apellido: {0} |", item.cliapellido);
                            Console.WriteLine("| Email: {0} |", item.cliemail);
                            Console.WriteLine("| Codigo: {0} |", item.codigo_cliente);
                            Console.WriteLine("| Empresa Contrato: {0} |", item.empresa_contrato);
                            Console.WriteLine("| Tipo Cliente: {0} |", item.tipo_cliente);
                            datasetcli.Remove(item);
                            Console.WriteLine("\n");
                            Console.WriteLine("DATOS DE CLIENTES HAN SIDO ELIMINADOS! ");
                            Console.WriteLine("\n");
                        }
                        else
                        {
                            Console.WriteLine("\n");
                            Console.WriteLine("NO SE ENCUENTRA CLIENTE EN LISTA!. ");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(" ");
            }

        }

        // METODO MODIFICAR DATOS CLIENTE //
        public void Modificar_Datos_cli()
        {
            try
            {
                if (Lista_Vacia_cli() == true)
                {
                    Console.WriteLine("\n");
                    Console.WriteLine("\n");
                    Console.WriteLine("No hay datos disponibles en la lista!. ");

                }
                else
                {
                    Clientes cli = new Clientes();
                    string buscarcli;
                    Console.WriteLine("Buscar Nombre Cliente para modificar: ");
                    buscarcli = Console.ReadLine();
                    foreach (var item in datasetcli)
                    {
                        if (buscarcli == item.clinombre)
                        {
                            Console.WriteLine("\n");
                            Console.WriteLine("\n");
                            Console.WriteLine("----------------------- MODIFICAR ---------------------------");
                            Console.WriteLine("| Nombre: {0} |", item.clinombre);
                            Console.WriteLine("| Apellido: {0} |", item.cliapellido);
                            Console.WriteLine("| Email: {0} |", item.cliemail);
                            Console.WriteLine("| Codigo: {0} |", item.codigo_cliente);
                            Console.WriteLine("| Empresa Contrato: {0} |", item.empresa_contrato);
                            Console.WriteLine("| Tipo Cliente: {0} |", item.tipo_cliente);
                            Console.WriteLine("--------------------------------------------------\n\n");

                            Console.WriteLine("Ingresar nuevo nombre: ");
                            cli.clinombre = Console.ReadLine();
                            item.clinombre = cli.clinombre;
                            Console.WriteLine("Ingresar nuevo apellido: ");
                            cli.cliapellido = Console.ReadLine();
                            item.cliapellido = cli.cliapellido;
                            Console.WriteLine("Ingresar nuevo email: ");
                            cli.cliemail = Console.ReadLine();
                            item.cliemail = cli.cliemail;
                            Console.WriteLine("Ingresar nuevo codigo: ");
                            cli.codigo_cliente = Convert.ToInt32(Console.ReadLine());
                            item.codigo_cliente = cli.codigo_cliente;
                            Console.WriteLine("Ingresar nueva empresa contrato: ");
                            cli.empresa_contrato = Console.ReadLine();
                            item.empresa_contrato = cli.empresa_contrato;
                            Console.WriteLine("Ingresar nuevo tipo de cliente: ");
                            cli.tipo_cliente = Console.ReadLine();
                            item.tipo_cliente = cli.tipo_cliente;
                            Console.WriteLine("--------------------------------------------------\n\n");
                            cli.NombreCli();
                            cli.ApellidoCli();
                            cli.EmailCli();
                            Console.WriteLine("--------------------------------------------------\n\n");
                            Console.WriteLine("LOS DATOS HAN SIDO MODIFICADOS CORRECTAMENTE! ");
                        }
                        else
                        {
                            Console.WriteLine("\n");

                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(" ");
            }

        }

        // METODO BUSCAR CLIENTE //
        public void Buscar_Cliente()
        {
            if (Lista_Vacia_cli() == true)
            {
                Console.WriteLine("\n");
                Console.WriteLine("\n");
                Console.WriteLine("No hay datos disponibles en la lista!. ");
            }
            else
            {
                string buscarcli;
                Console.WriteLine("\n");
                Console.WriteLine("\n");
                Console.WriteLine("Ingrese nombre de cliente a buscar. ");
                buscarcli = Console.ReadLine();
                foreach (Clientes item in datasetcli)
                {
                    if (buscarcli == item.clinombre)
                    {
                        Console.WriteLine("| Nombre: {0} |", item.clinombre);
                        Console.WriteLine("| Apellido: {0} |", item.cliapellido);
                        Console.WriteLine("| Email: {0} |", item.cliemail);
                        Console.WriteLine("| Codigo: {0} |", item.codigo_cliente);
                        Console.WriteLine("| Empresa Contrato: {0} |", item.empresa_contrato);
                        Console.WriteLine("| Tipo Cliente: {0} |", item.tipo_cliente);
                        Console.WriteLine("--------------------------------------------------\n\n");
                    }
                    else
                    {
                        Console.WriteLine("\n");
                    }
                }
            }
        }


    }
}
