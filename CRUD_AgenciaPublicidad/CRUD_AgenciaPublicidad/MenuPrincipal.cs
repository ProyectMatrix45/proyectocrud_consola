﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD_AgenciaPublicidad
{
    public class MenuPrincipal : ProcesosEjecutivos
    {

        // METODO INICIAR MENU PRINCIPAL //
        string op_menu = "";
        public void iniciar()
        {
            do
            {
                Console.WriteLine("\n");
                Console.WriteLine("\n");
                Console.WriteLine("---------------- MAIN MENU ----------------");
                Console.WriteLine("\n");
                Console.WriteLine(" SELECCIONE UNA OPCION DE SUB-MENU");
                Console.WriteLine("\n");
                Console.WriteLine("----------------------------------");
                Console.WriteLine("1. MENU EJECUTIVO. ");
                Console.WriteLine("2. MENU CLIENTE. ");
                Console.WriteLine("3. MENU ADMINISTRADOR/PROGRAMADOR. ");
                Console.WriteLine("0. SALIR ");
                Console.WriteLine("----------------------------------");
                op_menu = Console.ReadLine();
                seleccionar_submenu(op_menu);

            } while (op_menu != "0");
        }

        // METODO SELECCION OPCIONES SUBMENU //
        private void seleccionar_submenu(string op)
        {
            if (op == "")
            {
                return;
            }
            switch (op)
            {
                case "1":
                    Console.Clear();
                    MenuEjecutivo();
                    volver_menu();
                    Console.ReadKey();
                    break;
                case "2":
                    Console.Clear();
                    MenuCliente();
                    volver_menu();
                    Console.ReadKey();
                    break;
                case "3":
                    Console.Clear();
                    MenuAdmin();
                    volver_menu();
                    Console.ReadKey();
                    break;
                case "0":
                    Console.Clear();
                    Environment.Exit(0);
                    Console.ReadKey();
                    break;
                case "M":
                    Console.Clear();
                    iniciar();
                    Console.ReadKey();
                    break;
                default:
                    // Console.WriteLine("OPCION INVALIDA."); //
                    break;
            }
        }

        // METODO INICIALIZACION MENU EJECUTIVOS //
        private void MenuEjecutivo()
        {
            Console.WriteLine("\n");
            Console.WriteLine("\n");
            Console.WriteLine("----------------MENU CONTROL EJECUTIVOS----------------");
            Console.WriteLine("\n");
            Console.WriteLine(" SELECCIONE UNA OPCION DE EJECUTIVO. ");
            Console.WriteLine("\n");
            Console.WriteLine("----------------------------------");
            Console.WriteLine("1. AGREGAR EJECUTIVO. ");
            Console.WriteLine("2. LISTAR EJECUTIVO. ");
            Console.WriteLine("3. ELIMINAR EJECUTIVO. ");
            Console.WriteLine("4. MODIFICAR EJECUTIVO. ");
            Console.WriteLine("5. BUSCAR EJECUTIVOS. ");
            Console.WriteLine("0. VOLVER A MAIN MENU ");
            Console.WriteLine("----------------------------------");
            Console.WriteLine("\n");
            op_menu = Console.ReadLine();
            seleccionar_opcion(op_menu);
        }

        // METODO INICIALIZACION MENU CLIENTES //
        private void MenuCliente()
        {
            Console.WriteLine("\n");
            Console.WriteLine("\n");
            Console.WriteLine("----------------MENU CONTROL CLIENTE----------------");
            Console.WriteLine("\n");
            Console.WriteLine(" SELECCIONE UNA OPCION DE CLIENTE. ");
            Console.WriteLine("\n");
            Console.WriteLine("----------------------------------");
            Console.WriteLine("1. AGREGAR CLIENTE. ");
            Console.WriteLine("2. LISTAR CLIENTE. ");
            Console.WriteLine("3. ELIMINAR CLIENTE. ");
            Console.WriteLine("4. MODIFICAR CLIENTE. ");
            Console.WriteLine("5. BUSCAR CLIENTE. ");
            Console.WriteLine("0. VOLVER A MAIN MENU ");
            Console.WriteLine("----------------------------------");
            Console.WriteLine("\n");
            op_menu = Console.ReadLine();
            seleccionar_opcionCli(op_menu);
        }

        // METODO SELECCION DE OPCIONES MENU CLIENTES //
        private void seleccionar_opcionCli(string op)
        {
            if (op == "")
            {
                return;
            }
            switch (op)
            {
                case "1":
                    Console.Clear();
                    Crear_Cliente();
                    volver_menu();
                    Console.ReadKey();
                    break;
                case "2":
                    Console.Clear();
                    ListarCli();
                    volver_menu();
                    Console.ReadKey();
                    break;
                case "3":
                    Console.Clear();
                    Eliminar_Datos_cli();
                    volver_menu();
                    Console.ReadKey();
                    break;
                case "4":
                    Console.Clear();
                    Modificar_Datos_cli();
                    volver_menu();
                    Console.ReadKey();
                    break;
                case "5":
                    Console.Clear();
                    Buscar_Cliente();
                    volver_menu();
                    Console.ReadKey();
                    break;
                case "0":
                    Console.Clear();
                    iniciar();
                    // Environment.Exit(0); //
                    Console.ReadKey();
                    break;
                case "C":
                    Console.Clear();
                    MenuCliente();
                    Console.ReadKey();
                    break;
                default:
                    // Console.WriteLine("OPCION INVALIDA."); //
                    break;
            }
        }

        // METODO INICIALIZACION MENU ADMINISTRADOR/PROGRAMADOR //

        private void MenuAdmin()
        {
            Console.WriteLine("\n");
            Console.WriteLine("\n");
            Console.WriteLine("----------------MENU CONTROL ADMINISTRADOR----------------");
            Console.WriteLine("\n");
            Console.WriteLine(" SELECCIONE UNA OPCION DE ADMINISTRADOR. ");
            Console.WriteLine("\n");
            Console.WriteLine("----------------------------------");
            Console.WriteLine("1. AGREGAR ADMINISTRADOR. ");
            Console.WriteLine("2. LISTAR ADMINISTRADOR. ");
            Console.WriteLine("3. ELIMINAR ADMINISTRADOR. ");
            Console.WriteLine("4. MODIFICAR ADMINISTRADOR. ");
            Console.WriteLine("5. BUSCAR ADMINISTRADOR. ");
            Console.WriteLine("0. VOLVER A MAIN MENU ");
            Console.WriteLine("----------------------------------");
            Console.WriteLine("\n");
            op_menu = Console.ReadLine();
            seleccionar_opcionAdmin(op_menu);
        }

        // METODO SELECCION DE OPCIONES MENU ADMINISTRADOR //
        private void seleccionar_opcionAdmin(string op)
        {
            if (op == "")
            {
                return;
            }
            switch (op)
            {
                case "1":
                    Console.Clear();
                    Crear_Administrador();
                    volver_menu();
                    Console.ReadKey();
                    break;
                case "2":
                    Console.Clear();
                    ListarAdmin();
                    volver_menu();
                    Console.ReadKey();
                    break;
                case "3":
                    Console.Clear();
                    Eliminar_Datos_admin();
                    volver_menu();
                    Console.ReadKey();
                    break;
                case "4":
                    Console.Clear();
                    Modificar_Datos_admin();
                    volver_menu();
                    Console.ReadKey();
                    break;
                case "5":
                    Console.Clear();
                    Buscar_Admin();
                    volver_menu();
                    Console.ReadKey();
                    break;
                case "0":
                    Console.Clear();
                    iniciar();
                    // Environment.Exit(0); //
                    Console.ReadKey();
                    break;
                case "A":
                    Console.Clear();
                    MenuAdmin();
                    Console.ReadKey();
                    break;
                default:
                    // Console.WriteLine("OPCION INVALIDA."); //
                    break;
            }
        }

        // METODO SELECCION DE OPCIONES MENU EJECUTIVO //
        private void seleccionar_opcion(string op)
        {
            if (op == "")
            {
                return;
            }
            switch (op)
            {
                case "1":
                    Console.Clear();
                    Crear_Ejecutivo();
                    volver_menu();
                    Console.ReadKey();
                    break;
                case "2":
                    Console.Clear();
                    Listar();
                    volver_menu();
                    Console.ReadKey();
                    break;
                case "3":
                    Console.Clear();
                    Eliminar_Datos();
                    volver_menu();
                    Console.ReadKey();
                    break;
                case "4":
                    Console.Clear();
                    Modificar_Datos();
                    volver_menu();
                    Console.ReadKey();
                    break;
                case "5":
                    Console.Clear();
                    Buscar_Ejecutivo();
                    volver_menu();
                    Console.ReadKey();
                    break;
                case "0":
                    Console.Clear();
                    iniciar();
                    // Environment.Exit(0); //
                    Console.ReadKey();
                    break;
                case "M":
                    Console.Clear();
                    MenuEjecutivo();
                    Console.ReadKey();
                    break;
                default:
                    // Console.WriteLine("OPCION INVALIDA."); //
                    break;
            }
        }

        // METODO VOLVER A MENU //
        private void volver_menu()
        {
            string op;
            Console.WriteLine("Presione M --> MENU EJECUTIVO ; C --> MENU CLIENTE ; A --> MENU ADMINISTRADOR");
            op = Console.ReadLine();

            if (op.ToUpper() == "M")
            {
                seleccionar_opcion(op);
            }
            if (op.ToUpper() == "C")
            {
                seleccionar_opcionCli(op);
            }
            if (op.ToUpper() == "A")
            {
                seleccionar_opcionAdmin(op);
            }

        }

    }
}
