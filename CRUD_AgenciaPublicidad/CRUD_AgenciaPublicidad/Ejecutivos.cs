﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace CRUD_AgenciaPublicidad
{
    // CRUD EJECTIVOS
    public class Ejecutivos
    {
        public string nombre { get; set; }
        public string apellido { get; set; }
        public string email { get; set; }
        public int codigo_ejecutivo { get; set; }
        public string es_jefe { get; set; }
        public string area_trabajo { get; set; }

        // CONSTRUCTOR VACIO
        public Ejecutivos()
        {
        }

        // CONSTRUCTOR EJECUTIVOS //
        public Ejecutivos(string enombre, string eapellido, string eemail, int ecodigo, string ejefe, string earea)
        {
            this.nombre = enombre;
            this.apellido = eapellido;
            this.email = eemail;
            this.codigo_ejecutivo = ecodigo;
            this.es_jefe = ejefe;
            this.area_trabajo = earea;
        }

        // METODO 1. VERIFICAR NOMBRE VALIDO. 
        public void Nombre()
        {
            if (nombre.Contains("1234567890"))
            {
                Console.WriteLine("\n");
                Console.WriteLine("por favor introduzca un Nombre valido!");
            }
            else
            {
                Console.WriteLine("\n");
                Console.WriteLine("Nombre es valido!");
            }
        }
        // FIN METODO 1. VERIFICAR NOMBRE VALIDO. 

        // METODO 2. VERIFICAR APELLIDO VALIDO. 
        public string Apellido()
        {
            if (apellido.Contains("1234567890"))
            {
                Console.WriteLine("\n");
                return ("por favor introduzca un Apellido valido!");
            }
            else
            {
                
                return ("Apellido es valido!");
            }
        }
        // FIN METODO 2. VERIFICAR APELLIDO VALIDO. 

        // METODO 3. VERIFICAR EMAIL VALIDO
        public bool Email()
        {
            string expresion;
            expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if (Regex.IsMatch(email, expresion))
            {
                if (Regex.Replace(email, expresion, string.Empty).Length == 0)
                {
                    Console.WriteLine("Email es valido !");
                    Console.WriteLine("\n");
                    return true;
                }
                else
                {
                    Console.WriteLine("Email no es valido !");
                    Console.WriteLine("\n");
                    return false;
                }
            }
            else
            {
                Console.WriteLine("\n");
                Console.WriteLine("Error algo salio mal, intente nuevamente. ");
                return false;
            }
        }
        // FIN METODO 3. VERIFICAR EMAIL VALIDO.


        // METODO ES JEFE //


        // METODO VERIFICAR CODIGO //

        

        
    }
}
