﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD_AgenciaPublicidad
{
    public abstract class ProcesosEjecutivos : ProcesosClientes
    {
        List<Ejecutivos> dataset = new List<Ejecutivos>();
        string nombre, apellido, email, area_trabajo, es_jefe;
        int codigo_ejecutivo;

        // METODO CREACION DE EJECUTIVOS //
        public void Crear_Ejecutivo()
        {
            Console.WriteLine("\n");
            Console.WriteLine("\n");
            Console.WriteLine("--------- MENU CREACION EJECUTIVOS ---------");
            Console.WriteLine("\n");
            Console.WriteLine("INGRESE NOMBRE EJECUTIVO: ");
            nombre = Console.ReadLine();
            Console.WriteLine("\n");
            Console.WriteLine("INGRESE APELLIDO EJECUTIVO: ");
            apellido = Console.ReadLine();
            Console.WriteLine("\n");
            Console.WriteLine("INGRESE EMAIL EJECUTIVO: ");
            email = Console.ReadLine();
            Console.WriteLine("\n");
            Console.WriteLine("INGRESE CODIGO EJECUTIVO: ");
            codigo_ejecutivo = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("\n");
            Console.WriteLine("INGRESE AREA DE TRABAJO: ");
            area_trabajo = Console.ReadLine();
            Console.WriteLine("\n");
            Console.WriteLine("DETERMINE SI EJECUTIVO ES JEFE O NO, INGRESE [Y/N]: ");
            es_jefe = Console.ReadLine();
            dataset.Add(new Ejecutivos(nombre, apellido, email, codigo_ejecutivo, area_trabajo, es_jefe));
            Console.WriteLine("\n");
            Console.WriteLine("DATOS DE EJECUTIVOS HAN SIDO GUARDADOS! ");
        }

        // METODOS DE LISTAS EJECUTIVOS //
        private bool Lista_Vacia()
        {
            if (dataset.Count == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        ///////////////////////////////////////////////////////////
        public void Listar()
        {
            if (Lista_Vacia() == true)
            {
                Console.WriteLine("\n");
                Console.WriteLine("\n");
                Console.WriteLine("No hay datos disponibles en la lista!. ");

            }
            else
            {
                Console.WriteLine("\n");
                Console.WriteLine("TOTAL EJECUTIVOS:  " + dataset.Count);
                Console.WriteLine("\n");
                Console.WriteLine(" ------------------------- LISTA ------------------------- ");
                foreach (Ejecutivos item in dataset)
                {
                    Mostrar_Datos(item);
                }

            }
            Console.WriteLine("\n");
        }

        //  METODO MOSTRAR DATOS EJECUTIVO //
        public void Mostrar_Datos(Ejecutivos dato)
        {
            Console.WriteLine("\n");
            Console.WriteLine("--------------------------------------------------");
            Console.WriteLine("| Nombre: {0} |", dato.nombre);
            Console.WriteLine("| Apellido: {0} |", dato.apellido);
            Console.WriteLine("| Email: {0} |", dato.email);
            Console.WriteLine("| Codigo: {0} |", dato.codigo_ejecutivo);
            Console.WriteLine("| Jefe: {0} |", dato.area_trabajo);
            Console.WriteLine("| Area: {0} |", dato.es_jefe);
            dato.Nombre();
            dato.Apellido();
            dato.Email();
        }

        // METODO ELIMINAR DATOS EJECUTIVO //
        public void Eliminar_Datos()
        {
            try
            {
                string buscar;
                if (Lista_Vacia() == true)
                {
                    Console.WriteLine("\n");
                    Console.WriteLine("\n");
                    Console.WriteLine("No hay datos disponibles en la lista!. ");
                }
                else
                {
                    Console.WriteLine("Buscar Nombre Ejecutivo para eliminar: ");
                    buscar = Console.ReadLine();
                    foreach (var item in dataset)
                    {
                        if (buscar == item.nombre)
                        {
                            Console.WriteLine("--------------------------------------------------");
                            Console.WriteLine("| Nombre: {0} |", item.nombre);
                            Console.WriteLine("| Apellido: {0} |", item.apellido);
                            Console.WriteLine("| Email: {0} |", item.email);
                            Console.WriteLine("| Codigo: {0} |", item.codigo_ejecutivo);
                            Console.WriteLine("| Jefe: {0} |", item.area_trabajo);
                            Console.WriteLine("| Area: {0} |", item.es_jefe);
                            dataset.Remove(item);
                            Console.WriteLine("\n");
                            Console.WriteLine("DATOS DE EJECUTIVOS HAN SIDO ELIMINADOS! ");
                            Console.WriteLine("\n");
                        }
                        else
                        {
                            Console.WriteLine("\n");
                            Console.WriteLine("NO SE ENCUENTRA EJECUTIVO EN LISTA!. ");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(" ");
            }

        }

        // METODO MODIFICAR DATOS EJECUTIVO //
        public void Modificar_Datos()
        {
            try
            {
                if (Lista_Vacia() == true)
                {
                    Console.WriteLine("\n");
                    Console.WriteLine("\n");
                    Console.WriteLine("No hay datos disponibles en la lista!. ");

                }
                else
                {
                    Ejecutivos ej = new Ejecutivos();
                    string buscar;
                    Console.WriteLine("Buscar Nombre Ejecutivo para modificar: ");
                    buscar = Console.ReadLine();
                    foreach (var item in dataset)
                    {
                        if (buscar == item.nombre)
                        {
                            Console.WriteLine("\n");
                            Console.WriteLine("\n");
                            Console.WriteLine("----------------------- MODIFICAR ---------------------------");
                            Console.WriteLine("| Nombre: {0} |", item.nombre);
                            Console.WriteLine("| Apellido: {0} |", item.apellido);
                            Console.WriteLine("| Email: {0} |", item.email);
                            Console.WriteLine("| Codigo: {0} |", item.codigo_ejecutivo);
                            Console.WriteLine("| Jefe: {0} |", item.area_trabajo);
                            Console.WriteLine("| Area: {0} |", item.es_jefe);
                            Console.WriteLine("--------------------------------------------------\n\n");

                            Console.WriteLine("Ingresar nuevo nombre: ");
                            ej.nombre = Console.ReadLine();
                            item.nombre = ej.nombre;
                            Console.WriteLine("Ingresar nuevo apellido: ");
                            ej.apellido = Console.ReadLine();
                            item.apellido = ej.apellido;
                            Console.WriteLine("Ingresar nuevo email: ");
                            ej.email = Console.ReadLine();
                            item.email = ej.email;
                            Console.WriteLine("Ingresar nuevo codigo: ");
                            ej.codigo_ejecutivo = Convert.ToInt32(Console.ReadLine());
                            item.codigo_ejecutivo = ej.codigo_ejecutivo;
                            Console.WriteLine("Ingresar nueva area de trabajo: ");
                            ej.es_jefe = Console.ReadLine();
                            item.es_jefe = ej.es_jefe;
                            Console.WriteLine("Asignar jefe [Y/N]: ");
                            ej.area_trabajo = Console.ReadLine();
                            item.area_trabajo = ej.area_trabajo;
                            Console.WriteLine("--------------------------------------------------\n\n");
                            ej.Nombre();
                            ej.Apellido();
                            ej.Email();
                            Console.WriteLine("--------------------------------------------------\n\n");
                            Console.WriteLine("LOS DATOS HAN SIDO MODIFICADOS CORRECTAMENTE! ");
                        }
                        else
                        {
                            Console.WriteLine("\n");
                            
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(" ");
            }

        }

        // METODO BUSCAR EJECUTIVO //
        public void Buscar_Ejecutivo()
        {
            if (Lista_Vacia() == true)
            {
                Console.WriteLine("\n");
                Console.WriteLine("\n");
                Console.WriteLine("No hay datos disponibles en la lista!. ");
            }
            else
            {
                string buscar;
                Console.WriteLine("\n");
                Console.WriteLine("\n");
                Console.WriteLine("Ingrese nombre de ejecutivo a buscar. ");
                buscar = Console.ReadLine();
                foreach (Ejecutivos item in dataset)
                {
                    if (buscar == item.nombre)
                    {
                        Console.WriteLine("| Nombre: {0} |", item.nombre);
                        Console.WriteLine("| Apellido: {0} |", item.apellido);
                        Console.WriteLine("| Email: {0} |", item.email);
                        Console.WriteLine("| Codigo: {0} |", item.codigo_ejecutivo);
                        Console.WriteLine("| Jefe: {0} |", item.area_trabajo);
                        Console.WriteLine("| Area: {0} |", item.es_jefe);
                        Console.WriteLine("--------------------------------------------------\n\n");
                    }
                    else
                    {
                        Console.WriteLine("\n");
                    }
                }
            }
        }
    }
}
