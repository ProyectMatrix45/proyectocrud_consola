﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CRUD_AgenciaPublicidad
{
    // CRUD ADMINISTRADOR/PROGRAMADOR
    public class Administrador
    {

        public string adnombre { get; set; }
        public string adapellido { get; set; }
        public string ademail { get; set; }
        public int codigo_administrador { get; set; }
        public string es_administrador { get; set; }
        

        // CONSTRUCTOR VACIO
        public Administrador()
        {
        }

        // CONSTRUCTOR ADMINISTRADOR //
        public Administrador(string adminnombre, string adminapellido, string adminemail, int adcodigo, string admin)
        {
            this.adnombre = adminnombre;
            this.adapellido = adminapellido;
            this.ademail = adminemail;
            this.codigo_administrador = adcodigo;
            this.es_administrador = admin;
            
        }

        // METODO 1. VERIFICAR NOMBRE VALIDO. 
        public void NombreAdmin()
        {
            if (adnombre.Contains("1234567890"))
            {
                Console.WriteLine("\n");
                Console.WriteLine("por favor introduzca un Nombre valido!");
            }
            else
            {
                Console.WriteLine("\n");
                Console.WriteLine("Nombre es valido!");
            }
        }
        // FIN METODO 1. VERIFICAR NOMBRE VALIDO. 

        // METODO 2. VERIFICAR APELLIDO VALIDO. 
        public string ApellidoAdmin()
        {
            if (adapellido.Contains("1234567890"))
            {
                Console.WriteLine("\n");
                return ("por favor introduzca un Apellido valido!");
            }
            else
            {

                return ("Apellido es valido!");
            }
        }
        // FIN METODO 2. VERIFICAR APELLIDO VALIDO. 

        // METODO 3. VERIFICAR EMAIL VALIDO
        public bool EmailAdmin()
        {
            string expresion;
            expresion = "\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*";
            if (Regex.IsMatch(ademail, expresion))
            {
                if (Regex.Replace(ademail, expresion, string.Empty).Length == 0)
                {
                    Console.WriteLine("Email es valido !");
                    Console.WriteLine("\n");
                    return true;
                }
                else
                {
                    Console.WriteLine("Email no es valido !");
                    Console.WriteLine("\n");
                    return false;
                }
            }
            else
            {
                Console.WriteLine("\n");
                Console.WriteLine("Error algo salio mal, intente nuevamente. ");
                return false;
            }
        }
        // FIN METODO 3. VERIFICAR EMAIL VALIDO.

        // METODO VERIFICAR CODIGO //





    }
}
