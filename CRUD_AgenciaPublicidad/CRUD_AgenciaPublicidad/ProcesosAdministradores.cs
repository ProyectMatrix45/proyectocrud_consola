﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD_AgenciaPublicidad
{
    public abstract class ProcesosAdministradores
    {
        List<Administrador> datasetadmin = new List<Administrador>();
        string adminnombre, adminapellido, adminemail, es_administrador;
        int codigo_administrador;

        // METODO CREACION DE ADMINISTRADORES //
        public void Crear_Administrador()
        {
            Console.WriteLine("\n");
            Console.WriteLine("\n");
            Console.WriteLine("--------- MENU CREACION ADMINISTRADOR ---------");
            Console.WriteLine("\n");
            Console.WriteLine("INGRESE NOMBRE ADMINISTRADOR: ");
            adminnombre = Console.ReadLine();
            Console.WriteLine("\n");
            Console.WriteLine("INGRESE APELLIDO ADMINISTRADOR: ");
            adminapellido = Console.ReadLine();
            Console.WriteLine("\n");
            Console.WriteLine("INGRESE EMAIL ADMINISTRADOR: ");
            adminemail = Console.ReadLine();
            Console.WriteLine("\n");
            Console.WriteLine("INGRESE CODIGO ADMINISTRADOR: ");
            codigo_administrador = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("\n");
            Console.WriteLine("DETERMINE SI ADMINISTRADOR ES JEFE O NO, INGRESE [Y/N]: ");
            es_administrador = Console.ReadLine();
            datasetadmin.Add(new Administrador(adminnombre, adminapellido, adminemail, codigo_administrador, es_administrador));
            Console.WriteLine("\n");
            Console.WriteLine("DATOS DE ADMINISTRADOR HAN SIDO GUARDADOS! ");
        }

        // METODOS DE LISTAS ADMINISTRADOR //
        private bool Lista_Vacia_admin()
        {
            if (datasetadmin.Count == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        ///////////////////////////////////////////////////////////
        public void ListarAdmin()
        {
            if (Lista_Vacia_admin() == true)
            {
                Console.WriteLine("\n");
                Console.WriteLine("\n");
                Console.WriteLine("No hay datos disponibles en la lista!. ");

            }
            else
            {
                Console.WriteLine("\n");
                Console.WriteLine("TOTAL ADMINISTRADORES:  " + datasetadmin.Count);
                Console.WriteLine("\n");
                Console.WriteLine(" ------------------------- LISTA ------------------------- ");
                foreach (Administrador item in datasetadmin)
                {
                    Mostrar_Datos_admin(item);
                }

            }
            Console.WriteLine("\n");
        }

        //  METODO MOSTRAR DATOS ADMINISTRADOR //
        public void Mostrar_Datos_admin(Administrador datoadmin)
        {
            Console.WriteLine("\n");
            Console.WriteLine("--------------------------------------------------");
            Console.WriteLine("| Nombre: {0} |", datoadmin.adnombre);
            Console.WriteLine("| Apellido: {0} |", datoadmin.adapellido);
            Console.WriteLine("| Email: {0} |", datoadmin.ademail);
            Console.WriteLine("| Codigo: {0} |", datoadmin.codigo_administrador);
            Console.WriteLine("| Jefe: {0} |", datoadmin.es_administrador);
            datoadmin.NombreAdmin();
            datoadmin.ApellidoAdmin();
            datoadmin.EmailAdmin();
        }

        // METODO ELIMINAR DATOS ADMINISTRADOR //
        public void Eliminar_Datos_admin()
        {
            try
            {
                string buscaradmin;
                if (Lista_Vacia_admin() == true)
                {
                    Console.WriteLine("\n");
                    Console.WriteLine("\n");
                    Console.WriteLine("No hay datos disponibles en la lista!. ");
                }
                else
                {
                    Console.WriteLine("Buscar Nombre Administrador para eliminar: ");
                    buscaradmin = Console.ReadLine();
                    foreach (var item in datasetadmin)
                    {
                        if (buscaradmin == item.adnombre)
                        {
                            Console.WriteLine("--------------------------------------------------");
                            Console.WriteLine("| Nombre: {0} |", item.adnombre);
                            Console.WriteLine("| Apellido: {0} |", item.adapellido);
                            Console.WriteLine("| Email: {0} |", item.ademail);
                            Console.WriteLine("| Codigo: {0} |", item.codigo_administrador);
                            Console.WriteLine("| Jefe: {0} |", item.es_administrador);
                            datasetadmin.Remove(item);
                            Console.WriteLine("\n");
                            Console.WriteLine("DATOS DE ADMINISTRADOR HAN SIDO ELIMINADOS! ");
                            Console.WriteLine("\n");
                        }
                        else
                        {
                            Console.WriteLine("\n");
                            Console.WriteLine("NO SE ENCUENTRA ADMINISTRADOR EN LISTA!. ");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(" ");
            }

        }

        // METODO MODIFICAR DATOS ADMINISTRADOR //
        public void Modificar_Datos_admin()
        {
            try
            {
                if (Lista_Vacia_admin() == true)
                {
                    Console.WriteLine("\n");
                    Console.WriteLine("\n");
                    Console.WriteLine("No hay datos disponibles en la lista!. ");

                }
                else
                {
                    Administrador admin = new Administrador();
                    string buscaradmin;
                    Console.WriteLine("Buscar Nombre Administrador para modificar: ");
                    buscaradmin = Console.ReadLine();
                    foreach (var item in datasetadmin)
                    {
                        if (buscaradmin == item.adnombre)
                        {
                            Console.WriteLine("\n");
                            Console.WriteLine("\n");
                            Console.WriteLine("----------------------- MODIFICAR ---------------------------");
                            Console.WriteLine("| Nombre: {0} |", item.adnombre);
                            Console.WriteLine("| Apellido: {0} |", item.adapellido);
                            Console.WriteLine("| Email: {0} |", item.ademail);
                            Console.WriteLine("| Codigo: {0} |", item.codigo_administrador);
                            Console.WriteLine("| Jefe: {0} |", item.es_administrador);
                            Console.WriteLine("--------------------------------------------------\n\n");

                            Console.WriteLine("Ingresar nuevo nombre: ");
                            admin.adnombre = Console.ReadLine();
                            item.adnombre = admin.adnombre;
                            Console.WriteLine("Ingresar nuevo apellido: ");
                            admin.adapellido = Console.ReadLine();
                            item.adapellido = admin.adapellido;
                            Console.WriteLine("Ingresar nuevo email: ");
                            admin.ademail = Console.ReadLine();
                            item.ademail = admin.ademail;
                            Console.WriteLine("Ingresar nuevo codigo: ");
                            admin.codigo_administrador = Convert.ToInt32(Console.ReadLine());
                            item.codigo_administrador = admin.codigo_administrador;
                            Console.WriteLine("Asignar jefe [Y/N]: ");
                            admin.es_administrador = Console.ReadLine();
                            item.es_administrador = admin.es_administrador;
                            Console.WriteLine("--------------------------------------------------\n\n");
                            admin.NombreAdmin();
                            admin.ApellidoAdmin();
                            admin.EmailAdmin();
                            Console.WriteLine("--------------------------------------------------\n\n");
                            Console.WriteLine("LOS DATOS HAN SIDO MODIFICADOS CORRECTAMENTE! ");
                        }
                        else
                        {
                            Console.WriteLine("\n");

                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(" ");
            }

        }

        // METODO BUSCAR ADMINISTRADOR //
        public void Buscar_Admin()
        {
            if (Lista_Vacia_admin() == true)
            {
                Console.WriteLine("\n");
                Console.WriteLine("\n");
                Console.WriteLine("No hay datos disponibles en la lista!. ");
            }
            else
            {
                string buscaradmin;
                Console.WriteLine("\n");
                Console.WriteLine("\n");
                Console.WriteLine("Ingrese nombre de Administrador a buscar. ");
                buscaradmin = Console.ReadLine();
                foreach (Administrador item in datasetadmin)
                {
                    if (buscaradmin == item.adnombre)
                    {
                        Console.WriteLine("| Nombre: {0} |", item.adnombre);
                        Console.WriteLine("| Apellido: {0} |", item.adapellido);
                        Console.WriteLine("| Email: {0} |", item.ademail);
                        Console.WriteLine("| Codigo: {0} |", item.codigo_administrador);
                        Console.WriteLine("| Jefe: {0} |", item.es_administrador);
                        Console.WriteLine("--------------------------------------------------\n\n");
                    }
                    else
                    {
                        Console.WriteLine("\n");
                    }
                }
            }
        }


    }
}
